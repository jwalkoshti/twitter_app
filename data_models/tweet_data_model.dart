import 'package:flutter/cupertino.dart';

class TweetData {
  String fullName;
  String userName;
  String tweetString;
  String replyingTo;
  DateTime tweetCreated;
  List<TweetData> commentsList;
  int reTweetCount;
  int likedCount;
  bool isLiked;
  bool isReTweeted;
  bool isAComment;
  bool isShared;
  TweetData({
    @required this.isLiked,
    @required this.isReTweeted,
    @required this.isAComment,
    @required this.isShared,
    @required this.tweetString,
    @required this.fullName,
    @required this.userName,
    @required this.commentsList,
    @required this.reTweetCount,
    @required this.likedCount,
    @required this.tweetCreated,
    @required this.replyingTo,
  });
}

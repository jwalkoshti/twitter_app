import 'package:flutter/widgets.dart';

class User {
  String name;
  String userName;
  String eContact;
  String dateOfBirth;
  String joinedDate;
  String password;
  User({
    @required this.name,
    @required this.userName,
    @required this.dateOfBirth,
    @required this.eContact,
    @required this.joinedDate,
    @required this.password,
  });
}

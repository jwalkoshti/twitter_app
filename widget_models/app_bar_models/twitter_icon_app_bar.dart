import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/services/constants.dart';


class TwitterIcon extends StatelessWidget {
  const TwitterIcon({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      "assets/icons/twLogo.svg",
      color: kBlueColor,
      width: 28.0,
    );
  }
}

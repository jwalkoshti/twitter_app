import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';


class HeadingAppBar extends StatelessWidget {
  final String title;
  const HeadingAppBar({
    Key key, this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.7,
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          style: TextStyle(
            color: kBlackColor,
          ),
        ));
  }
}
import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';


class SearchBar extends StatelessWidget {
  final String hintText;
  final page;
  const SearchBar({
    Key key,
    this.hintText, this.page,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kEELGreyColor,
          border: Border.all(color: kELGreyColor),
          borderRadius: BorderRadius.circular(90.0)),
      height: 38,
      child: TextField(
          readOnly: true,
          onTap: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (c, a1, a2) => page,
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 100),
                reverseTransitionDuration: Duration(milliseconds: 100)
              ),
            );
          },
          decoration: InputDecoration(
              hintText: hintText,
              hintStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(left: 14, bottom: 13))),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';

import '../bottom_sheet_tile.dart';

class AnimatedReTweetButton extends StatefulWidget {
  final TweetData tweet;
  const AnimatedReTweetButton({
    Key key,
    this.tweet,
  }) : super(key: key);
  @override
  _AnimatedReTweetButtonState createState() => _AnimatedReTweetButtonState();
}

class _AnimatedReTweetButtonState extends State<AnimatedReTweetButton>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _offsetAnimation = TweenSequence([
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 0), end: Offset(0, -1.2)), weight: 1),
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 1.2), end: Offset(0, 0)), weight: 1)
    ]).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void numberAnimation() {
    _controller.forward().whenComplete(() => _controller.reset());
  }

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return TextButton.icon(
        onPressed: () {
          showModalBottomSheet(
            backgroundColor: Colors.transparent,
            context: context,
            builder: (context) => Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: !widget.tweet.isReTweeted
                  ? Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            width: 30,
                            height: 5,
                            decoration: BoxDecoration(
                                color: kELGreyColor,
                                borderRadius: BorderRadius.circular(90)),
                          ),
                        ),
                        BottomSheetTile(
                            text: "Retweet",
                            iconString: "assets/icons/reTweetIcon.svg",
                            ontap: () {
                              numberAnimation();
                              Navigator.pop(context);
                              dataServices.reTweetClicked(widget.tweet);
                            }),
                        BottomSheetTile(
                            text: "Quote Tweet",
                            iconString: "assets/icons/quoteTweetIcon.svg",
                            ontap: () {}),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    )
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            width: 30,
                            height: 5,
                            decoration: BoxDecoration(
                                color: kELGreyColor,
                                borderRadius: BorderRadius.circular(90)),
                          ),
                        ),
                        BottomSheetTile(
                            text: "Undo Retweet",
                            iconString: "assets/icons/reTweetIcon.svg",
                            ontap: () {
                              numberAnimation();
                              Navigator.pop(context);
                              dataServices.reTweetClicked(widget.tweet);
                            }),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
            ),
          );
        },
        icon: SvgPicture.asset(
          "assets/icons/reTweetIcon.svg",
          color: widget.tweet.isReTweeted ? kGreenColor : kDGreyColor,
          width: 20,
        ),
        label: SlideTransition(
          position: _offsetAnimation,
          child: Text(
            widget.tweet.reTweetCount.toString(),
            style: TextStyle(
                color: widget.tweet.isReTweeted ? kGreenColor : kDGreyColor,
                fontWeight: FontWeight.w400),
          ),
        ));
  }
}

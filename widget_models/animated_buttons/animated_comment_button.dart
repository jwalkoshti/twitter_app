import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/pages/tweet_pages/add_reply_screen.dart';
import 'package:twitter_application/services/constants.dart';

class AnimatedCommentButton extends StatefulWidget {
  final TweetData tweetData;
  final VoidCallback pageReturnAnimation;
  final VoidCallback pageAnimation;
  AnimatedCommentButton(
      {Key key, this.pageReturnAnimation, this.pageAnimation, this.tweetData})
      : super(key: key);

  @override
  _AnimatedCommentButtonState createState() => _AnimatedCommentButtonState();
}

class _AnimatedCommentButtonState extends State<AnimatedCommentButton>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _offsetAnimation = TweenSequence([
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 0), end: Offset(0, -1.2)), weight: 1),
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 1.2), end: Offset(0, 0)), weight: 1)
    ]).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void numberAnimation() {
    _controller.forward().whenComplete(() => _controller.reset());
  }



  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        onPressed: () {
          bottomToTopNavigation(
              context,
              AddReplyScreen(
                tweetData: widget.tweetData,
                initialInputText: "",
              ),
              );
        },
        icon: SvgPicture.asset(
          "assets/icons/replyIcon.svg",
          color: widget.tweetData.isAComment ? kBlueColor : kDGreyColor,
          width: 20,
        ),
        label: SlideTransition(
          position: _offsetAnimation,
          child: Text(
            widget.tweetData.commentsList.length.toString(),
            style: TextStyle(
                color: widget.tweetData.isAComment ? kDGreyColor : kDGreyColor,
                fontWeight: FontWeight.w400),
          ),
        ));
  }
}

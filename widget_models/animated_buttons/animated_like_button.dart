import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';


class AnimatedLikedButton extends StatefulWidget {
  final String iconString1;
  final String iconString2;
  final Color likedColor;
  final TweetData tweet;
  const AnimatedLikedButton({
    Key key,
    this.iconString1,
    this.iconString2,
    this.likedColor,
this.tweet,
  }) : super(key: key);
  @override
  _AnimatedLikedButtonState createState() => _AnimatedLikedButtonState();
}

class _AnimatedLikedButtonState extends State<AnimatedLikedButton>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _offsetAnimation = TweenSequence([
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 0), end: Offset(0, -1.2)), weight: 1),
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 1.2), end: Offset(0, 0)), weight: 1)
    ]).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void numberAnimation() {
    _controller.forward().whenComplete(() => _controller.reset());
  }

  @override
  Widget build(BuildContext context) {
    final _listData = Provider.of<DataServices>(context);
    return TextButton.icon(
          onPressed: () {
            numberAnimation();
            _listData.setLike(widget.tweet);
          },
          icon: !widget.tweet.isLiked
              ? SvgPicture.asset(
                  widget.iconString1,
                  color: kDGreyColor,
                  width: 20,
                )
              : SvgPicture.asset(
                  widget.iconString2,
                  color: widget.likedColor,
                  width: 20,
                ),
          label: SlideTransition(
            position: _offsetAnimation,
            child: Text(
              widget.tweet.likedCount.toString(),
              style: TextStyle(
                  color:widget.tweet.isLiked
                      ? widget.likedColor
                      : kDGreyColor,
                  fontWeight: FontWeight.w400),
            ),
          ),
    );
  }
}

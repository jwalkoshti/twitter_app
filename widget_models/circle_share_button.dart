import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/services/constants.dart';


class CircleShareButton extends StatelessWidget {
  final String iconString;
  final String buttonName;
  final VoidCallback onTap;
  const CircleShareButton({
    Key key,
    this.iconString,
    this.buttonName,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(100)),
          width: 55,
          height: 55,
          child: Material(
            elevation: 1,
            color: Colors.white,
            borderRadius: BorderRadius.circular(100),
            child: InkWell(
              borderRadius: BorderRadius.circular(100),
              onTap: onTap,
              child: Center(
                  child: SvgPicture.asset(
                iconString,
                color: kDGreyColor,
              )),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(buttonName),
        )
      ],
    );
  }
}
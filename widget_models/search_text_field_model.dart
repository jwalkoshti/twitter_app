import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';


class SearchTextField extends StatefulWidget {
  final String hintText;

  const SearchTextField({Key key, this.hintText}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new SearchTextFieldState();
  }
}

class SearchTextFieldState extends State<SearchTextField> {
  final TextEditingController _textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        new Expanded(
          child: Stack(alignment: Alignment(1.0, 1.0), children: <Widget>[
            new TextField(
              autofocus: true,
              style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.w400, color: kBlueColor),
              onTap: () {},
              decoration: InputDecoration(
                hintText: widget.hintText,
                hintStyle: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: kLGreyColor),
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(left: 14, bottom: 16),
              ),
              onChanged: (text) {
                setState(() {
                  print(text);
                });
              },
              controller: _textController,
            ),
            _textController.text.length > 0
                ? new IconButton(
                    icon: new Icon(
                      Icons.clear,
                      color: kBlueColor,
                    ),
                    onPressed: () {
                      setState(() {
                        _textController.clear();
                      });
                    })
                : new Container(
                    height: 0.0,
                  )
          ]),
        ),
      ],
    );
  }
}
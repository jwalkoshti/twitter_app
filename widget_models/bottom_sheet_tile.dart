import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/services/constants.dart';


class BottomSheetTile extends StatelessWidget {
  final String text;
  final String iconString;
  final VoidCallback ontap;
  const BottomSheetTile({
    Key key,
    this.text,
    this.iconString, this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: ontap,
        child: Container(
          height: 60,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Row(
            children: [
             Container(
               width: 40,
               child: SvgPicture.asset(iconString,color: kDGreyColor,)),
              SizedBox(
                width: 10,
              ),
              Text(
                text,
                style: TextStyle(color: kBlackColor, fontSize: 18),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
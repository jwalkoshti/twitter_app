import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/pages/tweet_pages/detail_tweet_page.dart';
import 'package:twitter_application/services/constants.dart';
import 'animated_buttons/animated_comment_button.dart';
import 'animated_buttons/animated_like_button.dart';
import 'animated_buttons/animated_retweet_button.dart';
import 'bottom_sheet_tile.dart';
import 'circle_share_button.dart';

class Tweet extends StatefulWidget {
  final TweetData tweetData;

  const Tweet({
    Key key,
    this.tweetData,
  }) : super(key: key);
  @override
  _TweetState createState() => _TweetState();
}

class _TweetState extends State<Tweet> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: kELGreyColor), color: Colors.white),
      child: Material(
        color: Colors.white,
        child: InkWell(
          onTap: () {
            rightToLeftNavigation(
              context,
              DetailTweetPage(tweet: widget.tweetData),
            );
          },
          child: Container(
            padding: EdgeInsets.only(top: 10, left: 10, right: 10),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //profile_picture
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(90),
                          color: kLGreyColor),
                      child: IconButton(
                        icon: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: 35,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // tweet person name
                          Row(
                            children: [
                              Text(
                                widget.tweetData.fullName,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              // tweet username
                              Text(
                                widget.tweetData.userName,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: kDGreyColor),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                width: 2,
                                height: 2,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: kDGreyColor),
                              ),
                              Text(
                                "${widget.tweetData.tweetCreated.hour}h",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: kDGreyColor),
                              ),
                            ],
                          ),
                          widget.tweetData.isAComment
                              ? Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Row(
                                    children: [
                                      Text(
                                        "Replying to ",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            color: kDGreyColor),
                                      ),
                                      Text(
                                        widget.tweetData.replyingTo,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            color: kBlueColor),
                                      )
                                    ],
                                  ),
                                )
                              : Container(),
                          Padding(
                            padding: const EdgeInsets.only(top: 5, left: 1),
                            child: Text(widget.tweetData.tweetString),
                          )
                        ],
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.more_vert,
                        size: 20,
                        color: kDGreyColor,
                      ),
                      onPressed: () {
                        showModalBottomSheet(
                          backgroundColor: Colors.transparent,
                          context: context,
                          builder: (context) => Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20))),
                            // height: MediaQuery.of(context).size.height * 0.35,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Center(
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    width: 30,
                                    height: 5,
                                    decoration: BoxDecoration(
                                        color: kELGreyColor,
                                        borderRadius:
                                            BorderRadius.circular(90)),
                                  ),
                                ),
                                BottomSheetTile(
                                    text: "Follow ${widget.tweetData.userName}",
                                    iconString: "assets/icons/followIcon.svg",
                                    ontap: () {}),
                                BottomSheetTile(
                                    text: "Add/remove from Lists",
                                    iconString: "assets/icons/addListIcon.svg",
                                    ontap: () {}),
                                BottomSheetTile(
                                    text: "Mute  ${widget.tweetData.userName}",
                                    iconString: "assets/icons/muteIcon.svg",
                                    ontap: () {}),
                                BottomSheetTile(
                                    text: "Block  ${widget.tweetData.userName}",
                                    iconString: "assets/icons/blockIcon.svg",
                                    ontap: () {}),
                                // Spacer(),
                                Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Divider(
                                      height: 1,
                                    )),
                                BottomSheetTile(
                                    text: "Report Tweet",
                                    iconString:
                                        "assets/icons/reportTweetIcon.svg",
                                    ontap: () {}),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(),
                    AnimatedCommentButton(
                      tweetData: widget.tweetData,
                    ),
                    AnimatedReTweetButton(tweet: widget.tweetData),
                    AnimatedLikedButton(
                      tweet: widget.tweetData,
                      iconString1: "assets/icons/likeIcon.svg",
                      iconString2: "assets/icons/likedIcon.svg",
                      likedColor: kPinkColor,
                    ),
                    // SHARE_BUTTON
                    IconButton(
                      onPressed: () {
                        showModalBottomSheet(
                          backgroundColor: Colors.transparent,
                          context: context,
                          builder: (context) => Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20))),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Center(
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    width: 30,
                                    height: 5,
                                    decoration: BoxDecoration(
                                        color: kELGreyColor,
                                        borderRadius:
                                            BorderRadius.circular(90)),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      "Share Tweet",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: kBlackColor),
                                    ),
                                  ),
                                ),
                                Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Divider(
                                      height: 1,
                                    )),
                                BottomSheetTile(
                                    text: "Send via Direct Message",
                                    iconString: "assets/icons/dmIcon.svg",
                                    ontap: () {}),
                                BottomSheetTile(
                                    text: "Share in a Fleet",
                                    iconString: "assets/icons/reTweetIcon.svg",
                                    ontap: () {}),
                                Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Divider(
                                      height: 1,
                                    )),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 20),
                                  child: Row(
                                    children: [
                                      CircleShareButton(
                                        iconString:
                                            "assets/icons/addToBookmarks.svg",
                                        buttonName: "Bookmark",
                                        onTap: () {},
                                      ),
                                      CircleShareButton(
                                        iconString:
                                            "assets/icons/copyLinkIcon.svg",
                                        buttonName: "Copy Link",
                                        onTap: () {},
                                      ),
                                      CircleShareButton(
                                        iconString:
                                            "assets/icons/shareIcon.svg",
                                        buttonName: "Share via",
                                        onTap: () {},
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Divider(
                                      height: 1,
                                    )),
                                SizedBox(
                                  height: 100,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      icon: SvgPicture.asset(
                        "assets/icons/shareIcon.svg",
                        color: widget.tweetData.isShared
                            ? kBlueColor
                            : kDGreyColor,
                        width: 20,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twitter_application/services/constants.dart';


class NotificationTile extends StatelessWidget {
  final String userName;
  final String name;
  final String notificationType;
  final VoidCallback onTap;
  const NotificationTile({
    Key key,
    this.userName,
    this.name,
    this.notificationType, this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            color: kELGreyColor,
            height: 1.0,
          ),
          Container(
            margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: SvgPicture.asset(
                        "assets/icons/selectedNotificationIcon.svg",
                        color: kBlueColor,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(90),
                          color: kLGreyColor),
                      child: IconButton(
                        icon: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: 20,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Spacer(),
                    IconButton(
                      icon: Icon(
                        Icons.more_vert,
                        size: 20,
                        color: kDGreyColor,
                      ),
                      onPressed: () {},
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  width: MediaQuery.of(context).size.width,
                  child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text: name,
                            style: TextStyle(
                                color: kBlackColor,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: notificationType,
                            style: TextStyle(
                                color: kBlackColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w400)),
                      ])),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/services/data_services.dart';

import 'application_home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DataServices>(
      create: (context) => DataServices(
          loggedInName: "Jwal Koshti", loggedInUserName: "@jwalkoshti"),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        onGenerateRoute: (RouteSettings settings) {
          if (settings.name == '/') {
            return PageRouteBuilder<dynamic>(
              transitionDuration: Duration(milliseconds: 300),
              reverseTransitionDuration: Duration(milliseconds: 300),
              pageBuilder: (BuildContext context, Animation<double> animation,
                      Animation<double> secondaryAnimation) =>
                  AppScreen(),
              transitionsBuilder: (
                BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child,
              ) {
                final Tween<double> scaleTween =
                    Tween<double>(begin: 1.0, end: 0.9);
                final Tween<double> fadeTween =
                    Tween<double>(begin: 1, end: 0.8);
                final Animation<double> _pageScaleAnimation =
                    scaleTween.animate(CurvedAnimation(
                        parent: secondaryAnimation, curve: Curves.easeOutSine));
                final Animation<double> _pageFadeAnimation = fadeTween.animate(
                    CurvedAnimation(
                        parent: secondaryAnimation, curve: Curves.easeInSine));
                return FadeTransition(
                  opacity: _pageFadeAnimation,
                  child: ScaleTransition(
                    scale: _pageScaleAnimation,
                    child: child,
                  ),
                );
              },
            );
          } else {
            // handle other routes here
            return null;
          }
        },
      ),
    );
  }
}

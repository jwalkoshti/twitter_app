import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

import 'add_username.dart';

class AddYourBio extends StatefulWidget {
  AddYourBio({Key key}) : super(key: key);

  @override
  _AddYourBioState createState() => _AddYourBioState();
}

class _AddYourBioState extends State<AddYourBio> {
  bool hasBio = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: kBlueColor),
        centerTitle: true,
        title: TwitterIcon(),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 20, left: 20),
              child: Text(
                "Describe yourself",
                style: TextStyle(
                    fontSize: 34,
                    fontWeight: FontWeight.bold,
                    color: kBlackColor),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, left: 20, right: 30),
              child: Text(
                "What makes you special? Don't think too hard, just have fun with it.",
                style: TextStyle(fontSize: 16, color: kDGreyColor),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 30),
              child: TextFormField(
                onChanged: (value) => {},
                maxLength: 160,
                style: TextStyle(color: kBlackColor, fontSize: 21),
                autofocus: true,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(0),
                    hintText: "Your bio",
                    hintStyle: TextStyle(color: kDGreyColor, fontSize: 21)),
              ),
            ),
            Spacer(),
            Container(
              height: 1,
              color: kELGreyColor,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () {
                        rightToLeftNavigation(context, AddYourUserName());
                      },
                      child: Text(
                        "Skip for now",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    width: 80,
                    height: 35,
                    child: Material(
                      color: hasBio ? kBlueColor : kBlueColor.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(90),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(90),
                        onTap: hasBio ? () {} : null,
                        child: Center(
                          child: Text("Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

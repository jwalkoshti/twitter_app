import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

import 'add_your_location.dart';

class AddYourUserName extends StatefulWidget {
  AddYourUserName({Key key}) : super(key: key);

  @override
  _AddYourUserNameState createState() => _AddYourUserNameState();
}

class _AddYourUserNameState extends State<AddYourUserName> {
  bool hasUserName = true;

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: kBlueColor),
        centerTitle: true,
        title: TwitterIcon(),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 20, left: 20),
              child: Text(
                "What should people call you?",
                style: TextStyle(
                    fontSize: 34,
                    fontWeight: FontWeight.bold,
                    color: kBlackColor),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, left: 20, right: 30),
              child: Text(
                "Your @username is unique. You can always change it later.",
                style: TextStyle(fontSize: 16, color: kDGreyColor),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 30),
              child: TextFormField(
                initialValue: dataServices.loggedInUserName,
                onChanged: (value) {
                  if (value.isEmpty) {
                    setState(() {
                      hasUserName = false;
                    });
                  } else {
                    setState(() {
                      hasUserName = true;
                    });
                    dataServices.loggedInUserName = value;
                  }
                },
                maxLength: 160,
                style: TextStyle(color: kBlackColor, fontSize: 21),
                autofocus: true,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(0),
                    hintText: "Enter a unique username",
                    hintStyle: TextStyle(color: kDGreyColor, fontSize: 21)),
              ),
            ),
            Spacer(),
            Container(
              height: 1,
              color: kELGreyColor,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () {
                        rightToLeftNavigation(context, AddYourLocation());
                      },
                      child: Text(
                        "Skip for now",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    width: 80,
                    height: 35,
                    child: Material(
                      color: hasUserName
                          ? kBlueColor
                          : kBlueColor.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(90),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(90),
                        onTap: hasUserName ? () {} : null,
                        child: Center(
                          child: Text("Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

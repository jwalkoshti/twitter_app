import 'package:flutter/material.dart';
import 'package:twitter_application/pages/edit_profile_pages/add_header-background.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';
import 'package:dotted_border/dotted_border.dart';

class AddProfilePicture extends StatefulWidget {
  AddProfilePicture({Key key}) : super(key: key);

  @override
  _AddProfilePictureState createState() => _AddProfilePictureState();
}

class _AddProfilePictureState extends State<AddProfilePicture> {
  bool isPhotoUploaded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: kBlueColor),
        centerTitle: true,
        title: TwitterIcon(),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 20, left: 20),
              child: Text(
                "Pick a profile picture",
                style: TextStyle(
                    fontSize: 34,
                    fontWeight: FontWeight.bold,
                    color: kBlackColor),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, left: 20),
              child: Text(
                "Have a favourite selfie? Upload it now.",
                style: TextStyle(fontSize: 18, color: kDGreyColor),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Center(
              child: GestureDetector(
                onTap: () {},
                child: DottedBorder(
                  dashPattern: [8],
                  borderType: BorderType.RRect,
                  strokeWidth: 2,
                  strokeCap: StrokeCap.round,
                  radius: Radius.circular(10),
                  color: kBlueColor,
                  child: Container(
                    height: 160,
                    width: 160,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.photo_camera,
                          color: kBlueColor,
                          size: 50,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Upload",
                          style: TextStyle(
                              color: kBlueColor,
                              fontSize: 22,
                              fontWeight: FontWeight.w600),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Spacer(),
            Container(
              height: 1,
              color: kELGreyColor,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () {
                        // pageAnimation();
                        rightToLeftNavigation(context, AddHeaderBackground());
                      },
                      child: Text(
                        "Skip for now",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    width: 80,
                    height: 35,
                    child: Material(
                      color: isPhotoUploaded
                          ? kBlueColor
                          : kBlueColor.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(90),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(90),
                        onTap: isPhotoUploaded ? () {} : null,
                        child: Center(
                          child: Text("Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/pages/edit_profile_pages/add_your_bio.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

class AddHeaderBackground extends StatefulWidget {
  AddHeaderBackground({Key key}) : super(key: key);

  @override
  _AddHeaderBackgroundState createState() => _AddHeaderBackgroundState();
}

class _AddHeaderBackgroundState extends State<AddHeaderBackground> {
  bool isPhotoUploaded = false;

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: kBlueColor),
        centerTitle: true,
        title: TwitterIcon(),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 20, left: 20),
              child: Text(
                "Pick a header",
                style: TextStyle(
                    fontSize: 34,
                    fontWeight: FontWeight.bold,
                    color: kBlackColor),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, left: 20, right: 30),
              child: Text(
                "People who visit your profile will see it. Show your style",
                style: TextStyle(fontSize: 16, color: kDGreyColor),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Center(
              child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: 250,
                    decoration: BoxDecoration(
                        border: Border.all(color: kDGreyColor),
                        borderRadius: BorderRadius.circular(10)),
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Stack(
                      children: [
                        Column(
                          children: [
                            Container(
                              height: 120,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                color: kELGreyColor,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.photo_camera,
                                    color: kBlueColor,
                                    size: 50,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Upload",
                                    style: TextStyle(
                                        color: kBlueColor,
                                        fontSize: 22,
                                        fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              height: 100,
                              color: Colors.white,
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, bottom: 30),
                          alignment: Alignment.bottomLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadiusDirectional.circular(
                                                100),
                                        color: Colors.white),
                                  ),
                                  Container(
                                      margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                                      height: 70,
                                      width: 70,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadiusDirectional.circular(
                                                  100),
                                          color: kLGreyColor),
                                      child: Icon(
                                        Icons.person_rounded,
                                        color: kDGreyColor,
                                        size: 60,
                                      )),
                                ],
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                dataServices.loggedInName,
                                style: TextStyle(
                                    fontSize: 21, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                dataServices.loggedInUserName,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: kDGreyColor),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )),
            ),
            Spacer(),
            Container(
              height: 1,
              color: kELGreyColor,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () {
                        // pageAnimation();
                        rightToLeftNavigation(context, AddYourBio());
                      },
                      child: Text(
                        "Skip for now",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    width: 80,
                    height: 35,
                    child: Material(
                      color: isPhotoUploaded
                          ? kBlueColor
                          : kBlueColor.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(90),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(90),
                        onTap: isPhotoUploaded ? () {} : null,
                        child: Center(
                          child: Text("Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

class FinalSubmitProfilePage extends StatefulWidget {
  FinalSubmitProfilePage({Key key}) : super(key: key);

  @override
  _FinalSubmitProfilePageState createState() => _FinalSubmitProfilePageState();
}

class _FinalSubmitProfilePageState extends State<FinalSubmitProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: kBlueColor),
          centerTitle: true,
          title: TwitterIcon(),
        ),
        body: Center(
          child: Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Your Profile has been updated",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                      color: kBlackColor),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 50,
                  child: Material(
                    color: kBlueColor,
                    borderRadius: BorderRadius.circular(90),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(90),
                      onTap: () {
                        int count = 0;
                        Navigator.of(context).popUntil((route) {
                          return count++ == 6;
                        });
                      },
                      child: Center(
                        child: Text("See Profile",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:twitter_application/services/constants.dart';

class AddMessageScreen extends StatefulWidget {
  AddMessageScreen({Key key}) : super(key: key);

  @override
  _AddMessageScreenState createState() => _AddMessageScreenState();
}

class _AddMessageScreenState extends State<AddMessageScreen> {
  bool _readOnly = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.arrow_back_sharp,
              color: kBlueColor,
            )),
        title: Text(
          "New message",
          style: TextStyle(color: kBlackColor),
        ),
        bottom: PreferredSize(
          child: Column(
            children: [
              Container(
                color: kELGreyColor,
                height: 1.0,
              ),
              Container(
                  color: kEELGreyColor,
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  child: Center(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: SvgPicture.asset(
                            "assets/icons/searchIcon.svg",
                            color: kDGreyColor,
                          ),
                        ),
                        Expanded(
                          child: TextField(
                            showCursor: true,
                            readOnly: _readOnly,
                            autofocus: true,
                            onTap: () {
                              setState(() {
                                _readOnly = false;
                              });
                            },
                            style: TextStyle(
                                fontSize: 21,
                                fontWeight: FontWeight.w300,
                                color: kBlackColor),
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Search for people and groups",
                                hintStyle: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w300,
                                    color: kDGreyColor)),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
          preferredSize: Size.fromHeight(60.0),
        ),
      ),
      body: Container(),
    );
  }
}

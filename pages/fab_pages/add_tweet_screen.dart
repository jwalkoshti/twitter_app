import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';

class AddTweet extends StatefulWidget {
  AddTweet({Key key}) : super(key: key);

  @override
  _AddTweetState createState() => _AddTweetState();
}

class _AddTweetState extends State<AddTweet> {
  bool isTextFieldEmpty;
  String userInputTweet;
  @override
  void initState() {
    super.initState();
    isTextFieldEmpty = true;
    userInputTweet = "";
  }

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: MediaQuery.of(context).size.height,
      child: AnimatedAlign(
        duration: Duration(milliseconds: 300),
        alignment: Alignment.topCenter,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/cancelButtonIcon.svg",
                color: kBlueColor,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                width: 80,
                child: Material(
                  color: isTextFieldEmpty
                      ? kBlueColor.withOpacity(0.5)
                      : kBlueColor,
                  borderRadius: BorderRadius.circular(90),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(90),
                    onTap: isTextFieldEmpty
                        ? null
                        : () => Navigator.pop(context,
                            dataServices.addToHomeTweetList(userInputTweet)),
                    child: Center(
                      child: Text("Tweet",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                  ),
                ),
              )
            ],
          ),
          body: Container(
            margin: EdgeInsets.only(top: 10),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.circular(100),
                          color: kLGreyColor),
                      child: Icon(
                        Icons.person_rounded,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        onChanged: (value) {
                          setState(() {
                            userInputTweet = value;
                            isTextFieldEmpty = value == "" ? true : false;
                          });
                        },
                        style: TextStyle(
                            fontSize: 21, fontWeight: FontWeight.w400),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "What's happening?",
                          hintStyle: TextStyle(
                              color: kDGreyColor,
                              fontSize: 18,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    )
                  ],
                ),
                Spacer(),
                Divider(
                  height: 1,
                ),
                InkWell(
                  onTap: () {},
                  splashColor: kEELGreyColor,
                  child: Container(
                      padding: EdgeInsets.only(left: 15),
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                            "assets/icons/globeIcon.svg",
                            color: kBlueColor,
                            width: 20,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Everyone can reply",
                            style: TextStyle(
                                color: kBlueColor, fontWeight: FontWeight.w400),
                          )
                        ],
                      )),
                ),
                Divider(
                  height: 1,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5, top: 0),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: SvgPicture.asset(
                          "assets/icons/addPicture.svg",
                          color: kBlueColor,
                          width: 25,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: SvgPicture.asset(
                          "assets/icons/addGIF.svg",
                          color: kBlueColor,
                          width: 25,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: SvgPicture.asset(
                          "assets/icons/addPoll.svg",
                          color: kBlueColor,
                          width: 25,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: SvgPicture.asset(
                          "assets/icons/addSchedule.svg",
                          color: kBlueColor,
                          width: 25,
                        ),
                      ),
                      Spacer(),
                      Container(
                        color: kLGreyColor,
                        height: 30,
                        width: 1,
                      ),
                      IconButton(
                          color: kBlueColor,
                          disabledColor: kBlueColor.withOpacity(0.5),
                          icon: Icon(Icons.add_circle),
                          onPressed: isTextFieldEmpty ? null : () {})
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

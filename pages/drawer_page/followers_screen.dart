import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:twitter_application/services/constants.dart';

class FollowersScreen extends StatelessWidget {
  const FollowersScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            borderRadius: BorderRadius.circular(90),
            child: Icon(
              Icons.arrow_back_rounded,
              color: kBlueColor,
            ),
          ),
          title: Text(
            "Followers",
            style: TextStyle(color: kBlackColor),
          ),
          actions: [
            InkWell(
              onTap: () {},
              borderRadius: BorderRadius.circular(90),
              child: Container(
                  width: 56,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: SvgPicture.asset("assets/icons/followIcon.svg",
                      color: kBlueColor)),
            ),
          ],
          bottom: PreferredSize(
            child: Container(
              color: kELGreyColor,
              height: 1.0,
            ),
            preferredSize: Size.fromHeight(1.0),
          ),
        ),
        body: ListView.builder(
          itemCount: 3,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: ListTile(
                    leading: Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadiusDirectional.circular(100),
                            color: kLGreyColor),
                        child: Icon(
                          Icons.person_rounded,
                          color: kDGreyColor,
                          size: 40,
                        )),
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Abc XYZ",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold, color: kBlackColor),
                        ),
                        Text("@abcXYZ", style: TextStyle(fontSize: 16, color: kDGreyColor),)
                      ],
                    ),
                    trailing: Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      width: 80,
                      child: Material(
                        color: kBlueColor,
                        borderRadius: BorderRadius.circular(90),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(90),
                          onTap: () {},
                          child: Center(
                            child: Text("Follow",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  color: kELGreyColor,
                  height: 1.0,
                ),
              ],
            );
          },
        )
        // Center(
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     crossAxisAlignment: CrossAxisAlignment.center,
        //     children: [
        //       Spacer(
        //         flex: 8,
        //       ),
        //       Container(
        //           padding: EdgeInsets.symmetric(horizontal: 10),
        //           child: Text(
        //             "You don't have any followers yet",
        //             style: TextStyle(
        //                 fontSize: 24,
        //                 fontWeight: FontWeight.bold,
        //                 color: kBlackColor),
        //             textAlign: TextAlign.center,
        //           )),
        //       Container(
        //         padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        //         child: Text(
        //           "When someone follows you, you'll see them here.",
        //           style: TextStyle(
        //               fontSize: 16,
        //               fontWeight: FontWeight.w400,
        //               color: kDGreyColor),
        //           textAlign: TextAlign.center,
        //         ),
        //       ),
        //       Spacer(
        //         flex: 9,
        //       )
        //     ],
        //   ),
        // ),
        );
  }
}

import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';


class BookmarksScreen extends StatefulWidget {
  const BookmarksScreen({
    Key key,
  }) : super(key: key);
  @override
  _BookmarksScreenState createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          borderRadius: BorderRadius.circular(90),
          child: Icon(
            Icons.arrow_back_rounded,
            color: kBlueColor,
          ),
        ),
        title: Text(
          "Bookmarks",
          style: TextStyle(color: kBlackColor),
        ),
        actions: [
          InkWell(
            onTap: () {},
            borderRadius: BorderRadius.circular(90),
            child: Container(
              width: 60,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                Icons.more_vert,
                color: kBlueColor,
                size: 28,
              ),
            ),
          ),
        ],
        bottom: PreferredSize(
          child: Container(
            color: kELGreyColor,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(
              flex: 4,
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  "You haven't added any Tweets to your Bookmarks yet",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: kBlackColor),
                  textAlign: TextAlign.center,
                )),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                "When you do, they'll show up here.",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: kDGreyColor),
              ),
            ),
            Spacer(
              flex: 5,
            )
          ],
        ),
      ),
    );
  }
}

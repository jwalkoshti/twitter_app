import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/pages/edit_profile_pages/add_profile_picture.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/tweet_model.dart';
import '../fab_pages/add_tweet_screen.dart';
import 'followers_screen.dart';
import 'following_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return SafeArea(
      child: DefaultTabController(
          length: profileScreenTabList.length,
          child: Scaffold(
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  // pageAnimation();
                  bottomToTopNavigation(context, AddTweet());
                },
                child: SvgPicture.asset(
                  "assets/icons/writeTweetIcon.svg",
                  color: Colors.white,
                ),
              ),
              body: NestedScrollView(
                  headerSliverBuilder: (context, innerBoxIsScrolled) {
                    return [
                      SliverAppBar(
                        elevation: 0,
                        pinned: true,
                        backgroundColor: kDBlueColor,
                        expandedHeight: 447,
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.parallax,
                          title: Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(dataServices.loggedInName),
                                Text(
                                  "0 Tweets",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                          ),
                          background: Stack(
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 190,
                                    color: kBlueColor,
                                  ),
                                  Container(
                                    height: 220,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, bottom: 50),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        GestureDetector(
                                          onTap: () {},
                                          child: Stack(
                                            children: [
                                              Container(
                                                height: 90,
                                                width: 90,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadiusDirectional
                                                            .circular(100),
                                                    color: Colors.white),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      5, 5, 0, 0),
                                                  height: 80,
                                                  width: 80,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadiusDirectional
                                                              .circular(100),
                                                      color: kLGreyColor),
                                                  child: Icon(
                                                    Icons.person_rounded,
                                                    color: kDGreyColor,
                                                    size: 60,
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 40),
                                          width: 120,
                                          height: 33,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(90),
                                              color: Colors.white,
                                              border: Border.all(
                                                  color: kBlueColor, width: 1)),
                                          child: Material(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(90),
                                            child: InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(90),
                                              onTap: () {
                                                // pageAnimation();
                                                rightToLeftNavigation(context,
                                                    AddProfilePicture());
                                              },
                                              child: Center(
                                                  child: Text(
                                                "Set up profile",
                                                style: TextStyle(
                                                    color: kBlueColor,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            dataServices.loggedInName,
                                            style: TextStyle(
                                                fontSize: 28,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            dataServices.loggedInUserName,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                color: kDGreyColor),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            children: [
                                              Icon(
                                                Icons.cake_rounded,
                                                size: 16,
                                                color: kDGreyColor,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8),
                                                  child: Text(
                                                    "Born 21 September 1999",
                                                    style: TextStyle(
                                                        color: kDGreyColor),
                                                  )),
                                              Icon(
                                                Icons.calendar_today,
                                                size: 16,
                                                color: kDGreyColor,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8),
                                                  child: Text(
                                                    "Joined May 2021",
                                                    style: TextStyle(
                                                        color: kDGreyColor),
                                                  )),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              TextButton.icon(
                                                  onPressed: () {
                                                    rightToLeftNavigation(
                                                        context,
                                                        FollowersScreen());
                                                  },
                                                  icon: Text(
                                                    "0",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: kBlackColor),
                                                  ),
                                                  label: Text(
                                                    "Followers",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: kDGreyColor),
                                                  )),
                                              TextButton.icon(
                                                  onPressed: () {
                                                    rightToLeftNavigation(
                                                        context,
                                                        FollowingScreen());
                                                  },
                                                  icon: Text(
                                                    "0",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: kBlackColor),
                                                  ),
                                                  label: Text(
                                                    "Following",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: kDGreyColor),
                                                  )),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        leading: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                              margin: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(90),
                                  color: kBlackColor.withOpacity(0.6)),
                              child: Icon(
                                Icons.arrow_back_sharp,
                                size: 20,
                              )),
                        ),
                        actions: [
                          Container(
                              width: 30,
                              margin: EdgeInsets.all(13),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(90),
                                  color: kBlackColor.withOpacity(0.6)),
                              child: Icon(
                                Icons.more_vert_rounded,
                                size: 24,
                              )),
                        ],
                        bottom: PreferredSize(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.white,
                              child: TabBar(
                                  isScrollable: true,
                                  labelStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                  physics: ScrollPhysics(),
                                  labelPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 0),
                                  unselectedLabelColor: kDGreyColor,
                                  labelColor: kBlueColor,
                                  indicatorWeight: 3,
                                  tabs: profileScreenTabList
                                      .map((item) => Tab(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10),
                                              child: Text(
                                                item,
                                                style: TextStyle(fontSize: 16),
                                                maxLines: 1,
                                              ),
                                            ),
                                          ))
                                      .toList()),
                            ),
                            preferredSize: Size.fromHeight(50)),
                      ),
                    ];
                  },
                  body: TabBarView(
                      children: profileScreenTabList.map(
                    (item) {
                      if (item == "Tweets") {
                        return Consumer<DataServices>(
                            builder: (context, dataServices, child) =>
                                ListView.builder(
                                  itemCount:
                                      dataServices.getMyTweetList().length,
                                  itemBuilder: (context, index) => Tweet(
                                    tweetData:
                                        dataServices.getMyTweetList()[index],
                                  ),
                                ));
                      } else if (item == "Tweets & replies") {
                        return Consumer<DataServices>(
                            builder: (context, dataServices, child) =>
                                ListView.builder(
                                  itemCount: dataServices
                                      .getMyCommentedTweetList()
                                      .length,
                                  itemBuilder: (context, index) => Tweet(
                                    tweetData: dataServices
                                        .getMyCommentedTweetList()[index],
                                  ),
                                ));
                      } else {
                        return Container();
                      }
                    },
                  ).toList())))),
    );
  }
}

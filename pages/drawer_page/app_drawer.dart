import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/pages/login_register_pages/login_page.dart';
import 'package:twitter_application/pages/login_register_pages/register_pages/register_page1.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';

import 'bookmarks_screen.dart';
import 'followers_screen.dart';
import 'following_screen.dart';
import 'profile_screen.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({
    Key key,
  }) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  bool _listVisibility = true;

  void navigationDrawerAnimation(Widget page) {
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
      rightToLeftNavigation(context, page);
    }
  }

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Drawer(
        child: Column(
          children: [
            Container(
              height: 240,
              child: DrawerHeader(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => navigationDrawerAnimation(ProfileScreen()),
                      child: Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadiusDirectional.circular(100),
                              color: kLGreyColor),
                          child: Icon(
                            Icons.person_rounded,
                            color: kDGreyColor,
                            size: 40,
                          )),
                    ),
                    ListTile(
                      onTap: () {
                        setState(() {
                          _listVisibility = !_listVisibility;
                        });
                      },
                      title: Text(
                        dataServices.loggedInName,
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        dataServices.loggedInUserName,
                        style: TextStyle(fontSize: 16),
                      ),
                      trailing: IconButton(
                        onPressed: () {
                          setState(() {
                            _listVisibility = !_listVisibility;
                          });
                        },
                        icon: Icon(
                          _listVisibility
                              ? Icons.keyboard_arrow_down_rounded
                              : Icons.keyboard_arrow_up_rounded,
                          color: kBlueColor,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextButton.icon(
                            onPressed: () =>
                                navigationDrawerAnimation(FollowingScreen()),
                            icon: Text(
                              "0",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: kBlackColor),
                            ),
                            label: Text(
                              "Following",
                              style:
                                  TextStyle(fontSize: 16, color: kDGreyColor),
                            )),
                        TextButton.icon(
                            onPressed: () =>
                                navigationDrawerAnimation(FollowersScreen()),
                            icon: Text(
                              "0",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: kBlackColor),
                            ),
                            label: Text(
                              "Followers",
                              style:
                                  TextStyle(fontSize: 16, color: kDGreyColor),
                            )),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Visibility(
              visible: _listVisibility,
              child: Column(
                children: [
                  ListTile(
                    onTap: () => navigationDrawerAnimation(ProfileScreen()),
                    leading: SvgPicture.asset(
                      "assets/icons/profileIcon.svg",
                      color: kDGreyColor,
                    ),
                    title: Text(
                      "Profile",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  ListTile(
                    onTap: () {},
                    leading: SvgPicture.asset(
                      "assets/icons/listsIcon.svg",
                      color: kDGreyColor,
                    ),
                    title: Text(
                      "Lists",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  ListTile(
                    onTap: () {},
                    leading: SvgPicture.asset(
                      "assets/icons/topicsIcon.svg",
                      color: kDGreyColor,
                    ),
                    title: Text(
                      "Topics",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  ListTile(
                    onTap: () => navigationDrawerAnimation(BookmarksScreen()),
                    leading: SvgPicture.asset(
                      "assets/icons/bookmarksIcon.svg",
                      color: kDGreyColor,
                    ),
                    title: Text(
                      "Bookmarks",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  ListTile(
                    onTap: () {},
                    leading: SvgPicture.asset(
                      "assets/icons/momentsIcon.svg",
                      color: kDGreyColor,
                    ),
                    title: Text(
                      "Moments",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(),
                  ListTile(
                    onTap: () {},
                    title: Text(
                      "Settings and Privacy",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  ListTile(
                    onTap: () {},
                    title: Text(
                      "Help Center",
                      style: TextStyle(
                          color: kBlackColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
                visible: !_listVisibility,
                child: Column(
                  children: [
                    ListTile(
                      onTap: () {
                        navigationDrawerAnimation(RegisterPage1());
                      },
                      title: Text(
                        "Create a new account",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        navigationDrawerAnimation(LogInPage());
                      },
                      title: Text(
                        "Add an existing account",
                        style: TextStyle(
                            color: kBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                )),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  padding: EdgeInsets.only(bottom: 5),
                  height: _listVisibility ? 100 : 0,
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                icon: Icon(
                                  Icons.lightbulb,
                                  color: kBlueColor,
                                ),
                                onPressed: () {}),
                            IconButton(
                                icon: Icon(
                                  Icons.qr_code,
                                  color: kBlueColor,
                                ),
                                onPressed: () {}),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

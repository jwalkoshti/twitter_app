import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/search_text_field_model.dart';


class SearchTwitterScreen extends StatefulWidget {
  @override
  _SearchTwitterScreenState createState() => _SearchTwitterScreenState();
}

class _SearchTwitterScreenState extends State<SearchTwitterScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          borderRadius: BorderRadius.circular(90),
          child: Icon(
            Icons.arrow_back_rounded,
            color: kBlueColor,
          ),
        ),
        title: Container(
          decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: Colors.transparent),
              borderRadius: BorderRadius.circular(90.0)),
          height: 38,
          child: SearchTextField(hintText: "Search Twitter"),
        ),
        bottom: PreferredSize(
          child: Container(
            color: kELGreyColor,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          Center(
              child: Text(
            "Try searching for people, topics or keywords",
            style: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w800, color: kDGreyColor),
          ))
        ],
      ),
    );
  }
}



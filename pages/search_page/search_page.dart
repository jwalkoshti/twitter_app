import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({
    Key key,
  }) : super(key: key);
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: searchScreenTabsList.length,
      child: Column(
        children: [
          TabBar(
              physics: ScrollPhysics(),
              labelPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              unselectedLabelColor: kDGreyColor,
              labelColor: kBlueColor,
              indicatorWeight: 3,
              isScrollable: true,
              tabs: searchScreenTabsList
                  .map((item) => Tab(
                        child: Text(
                          item,
                          style: TextStyle(fontSize: 16),
                        ),
                      ))
                  .toList()),
          Expanded(
              child: TabBarView(
                  children: searchScreenTabsList
                      .map(
                          (item) => Container(child: Center(child: Text(item))))
                      .toList()))
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/tweet_model.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kEELGreyColor,
      child: Consumer<DataServices>(
          builder: (context, dataServices, child) => ListView.builder(
                itemCount: dataServices.homeTweetList.length,
                itemBuilder: (context, index) => Tweet(
                  tweetData: dataServices.homeTweetList[index],
                ),
              )),
    );
  }
}

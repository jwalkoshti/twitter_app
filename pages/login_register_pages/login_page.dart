import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twitter_application/pages/login_register_pages/register_pages/register_page1.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  bool _obscureText;
  GlobalKey<FormState> _formKey;
  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _obscureText = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: kBlueColor),
        backgroundColor: Colors.white,
        actions: [
          TwitterIcon(),
          SizedBox(
            width: 40,
          ),
          TextButton(
              onPressed: () {
                rightToLeftNavigation(context, RegisterPage1());
              },
              child: Text(
                "Sign up",
                style: TextStyle(
                    color: kBlueColor,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              )),
          IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 30, bottom: 20),
            child: Text(
              "Log in to Twitter",
              style: TextStyle(
                  color: kBlackColor,
                  fontSize: 28,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter valid name";
                      }
                      return null;
                    },
                    onChanged: (value) => {},
                    maxLength: 50,
                    style: TextStyle(color: kBlackColor, fontSize: 21),
                    autofocus: true,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(0),
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        labelText: "Phone number, email address or username",
                        labelStyle:
                            TextStyle(color: kDGreyColor, fontSize: 21)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    obscureText: _obscureText,
                    onChanged: (value) => {},
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter a valid password";
                      }
                      return null;
                    },
                    style: TextStyle(color: kBlackColor, fontSize: 21),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(0),
                        suffixIcon: IconButton(
                            iconSize: 24,
                            color: _obscureText ? kLGreyColor : kBlueColor,
                            icon: Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            }),
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        labelText: "Password",
                        labelStyle:
                            TextStyle(color: kDGreyColor, fontSize: 21)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: TextButton(
                        onPressed: () {},
                        child: Text(
                          "Forgotten you password?",
                          style: TextStyle(
                              color: kDGreyColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        )),
                  )
                ],
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
          Container(
            height: 1,
            color: kELGreyColor,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              children: [
                Spacer(),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  width: 80,
                  height: 35,
                  child: Material(
                    color: kBlueColor,
                    borderRadius: BorderRadius.circular(90),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(90),
                      onTap: () {},
                      child: Center(
                        child: Text("Log in",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

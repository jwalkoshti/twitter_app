import 'package:flutter/material.dart';
import 'package:twitter_application/data_models/user_model.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';

class ConfirmationRegisterPage extends StatefulWidget {
  final User user;
  ConfirmationRegisterPage({Key key, this.user}) : super(key: key);

  @override
  _ConfirmationRegisterPageState createState() =>
      _ConfirmationRegisterPageState();
}

class _ConfirmationRegisterPageState extends State<ConfirmationRegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        iconTheme: IconThemeData(color: kBlueColor),
        backgroundColor: Colors.white,
        title: TwitterIcon(),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 10),
            child: Text(
              "Create your account",
              style: TextStyle(
                  color: kBlackColor,
                  fontSize: 34,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              children: [
                TextFormField(
                  initialValue: widget.user.name,
                  readOnly: true,
                  style: TextStyle(color: kBlackColor, fontSize: 18),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  initialValue: widget.user.eContact,
                  readOnly: true,
                  style: TextStyle(color: kBlackColor, fontSize: 18),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  initialValue: widget.user.dateOfBirth,
                  readOnly: true,
                  style: TextStyle(color: kBlackColor, fontSize: 18),
                ),
              ],
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 35),
            child: Column(
              children: [
                Text(
                  "By signing up, you agree to the Terms of Service and Privacy Policy, including Cookie Use. Others will be able to find you by email or phone number when provided · Privacy Options",
                  style: TextStyle(
                      fontSize: 16,
                      color: kDGreyColor,
                      fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 15, horizontal: 35),
            height: 50,
            child: Material(
              color: kBlueColor,
              borderRadius: BorderRadius.circular(90),
              child: InkWell(
                borderRadius: BorderRadius.circular(90),
                onTap: () {},
                child: Center(
                  child: Text("Sign up",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 21,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
        ],
      ),
    );
  }
}

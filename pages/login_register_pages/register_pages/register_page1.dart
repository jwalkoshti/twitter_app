import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twitter_application/data_models/user_model.dart';
import 'package:twitter_application/pages/login_register_pages/register_pages/confirmation_page.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/widget_models/app_bar_models/twitter_icon_app_bar.dart';
import 'package:intl/intl.dart';

class RegisterPage1 extends StatefulWidget {
  @override
  _RegisterPage1State createState() => _RegisterPage1State();
}

class _RegisterPage1State extends State<RegisterPage1> {
  User _user;
  GlobalKey<FormState> _formKey;
  bool isPhoneSelected;
  bool showDatePicker;
  bool isValid = false;
  TextEditingController dateController;
  FocusNode _focus = FocusNode();
  @override
  void initState() {
    super.initState();
    isPhoneSelected = true;
    showDatePicker = false;
    dateController = TextEditingController();
    _formKey = GlobalKey<FormState>();
    _user = User(
        dateOfBirth: "",
        name: "",
        userName: "",
        eContact: "",
        joinedDate:
            "${DateFormat.MMMM().format(DateTime.now())} ${DateFormat.y().format(DateTime.now())}",
        password: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        iconTheme: IconThemeData(color: kBlueColor),
        backgroundColor: Colors.white,
        title: TwitterIcon(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20),
            child: Text(
              "Create your account",
              style: TextStyle(
                  color: kBlackColor,
                  fontSize: 34,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter valid name";
                      }
                      return null;
                    },
                    onChanged: (value) => {_user.name = value},
                    onTap: () {
                      setState(() {
                        showDatePicker = false;
                      });
                    },
                    maxLength: 50,
                    style: TextStyle(color: kBlackColor, fontSize: 18),
                    autofocus: true,
                    decoration: InputDecoration(
                        hintText: "Name",
                        hintStyle: TextStyle(color: kDGreyColor, fontSize: 18)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    onChanged: (value) => {_user.eContact = value.toString()},
                    focusNode: _focus,
                    keyboardType: isPhoneSelected
                        ? TextInputType.number
                        : TextInputType.emailAddress,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length < 10) {
                        return isPhoneSelected
                            ? "Please enter valid phone number"
                            : "Please enter valid email";
                      }
                      return null;
                    },
                    onTap: () {
                      setState(() {
                        showDatePicker = false;
                      });
                    },
                    style: TextStyle(color: kBlackColor, fontSize: 18),
                    decoration: InputDecoration(
                        hintText: isPhoneSelected ? "Phone" : "Email",
                        hintStyle: TextStyle(color: kDGreyColor, fontSize: 18)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    readOnly: true,
                    controller: dateController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter Birth Date";
                      }
                      return null;
                    },
                    style: TextStyle(color: kBlackColor, fontSize: 18),
                    onFieldSubmitted: (value) {
                      _user.dateOfBirth = value;
                    },
                    decoration: InputDecoration(
                        hintText: "Date of birth",
                        hintStyle: TextStyle(color: kDGreyColor, fontSize: 18)),
                    onTap: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      setState(() {
                        showDatePicker = true;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
          Container(
            height: 1,
            color: kELGreyColor,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                showDatePicker
                    ? SizedBox(
                        height: 200,
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: CupertinoDatePicker(
                            onDateTimeChanged: (value) {
                              dateController.text =
                                  "${DateFormat.d().format(value)} ${DateFormat.MMMM().format(value)} ${DateFormat.y().format(value)}";
                            },
                            mode: CupertinoDatePickerMode.date,
                            initialDateTime: DateTime.now(),
                            minimumDate: DateTime(1900),
                            maximumDate: DateTime.now()),
                      )
                    : _focus.hasFocus
                        ? TextButton(
                            onPressed: () async {
                              _focus.unfocus();
                              setState(() {
                                isPhoneSelected = !isPhoneSelected;
                              });
                            },
                            child: Text(
                              !isPhoneSelected
                                  ? "Use phone instead"
                                  : "Use email instead",
                              style: TextStyle(
                                  color: kBlueColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400),
                            ))
                        : Container(),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  width: 80,
                  height: 35,
                  child: Material(
                    color: kBlueColor,
                    borderRadius: BorderRadius.circular(90),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(90),
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          _user.dateOfBirth = dateController.text;
                          rightToLeftNavigation(
                            context,
                            ConfirmationRegisterPage(user: _user),
                          );
                        }
                      },
                      child: Center(
                        child: Text("Next",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

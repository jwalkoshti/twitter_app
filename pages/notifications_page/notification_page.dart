import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';

import 'all_tab.dart';
import 'mentions_tab.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key key}) : super(key: key);
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: notificationScreenTabsList.length,
      child: Column(
        children: [
          TabBar(
              physics: ScrollPhysics(),
              labelPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              unselectedLabelColor: kDGreyColor,
              labelColor: kBlueColor,
              indicatorWeight: 3,
              tabs: notificationScreenTabsList
                  .map((item) => Tab(
                        child: Text(
                          item,
                          style: TextStyle(fontSize: 16),
                        ),
                      ))
                  .toList()),
          Expanded(
              child: TabBarView(
                  children: notificationScreenTabsList.map((item) {
            if (item == "Mentions") {
              return Mentions();
            } else if (item == "All") {
              return AllNotifications();
            }
          }).toList()))
        ],
      ),
    );
  }
}

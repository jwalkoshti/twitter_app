import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/pages/tweet_pages/detail_tweet_page.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/notification_tile.dart';

class Mentions extends StatelessWidget {
  const Mentions({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Consumer<DataServices>(
          builder: (context, dataServices, child) => ListView.builder(
            itemCount:
                dataServices.getMyMentionsNotificationList("@zebraXYZ").length,
            itemBuilder: (context, index) => NotificationTile(
              userName: "@zebraXYZ",
              name: "Zebra ",
              onTap: () {
                rightToLeftNavigation(
                  context,
                  DetailTweetPage(
                      tweet: dataServices
                          .getMyMentionsNotificationList("@zebraXYZ")[index]),
                );
              },
              notificationType: "has liked your Tweet",
            ),
          ),
        ),
      ),

      // Column(
      //   mainAxisAlignment: MainAxisAlignment.center,
      //   crossAxisAlignment: CrossAxisAlignment.center,
      //   children: [
      //     Spacer(
      //       flex: 9,
      //     ),
      //     Container(
      //         padding: EdgeInsets.symmetric(horizontal: 10),
      //         child: Text(
      //           "Nothing to see here... yet.",
      //           style: TextStyle(
      //               fontSize: 24,
      //               fontWeight: FontWeight.bold,
      //               color: kBlackColor),
      //           textAlign: TextAlign.center,
      //         )),
      //     Container(
      //       padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 10),
      //       child: Text(
      //         "When someone mentions you in a Tweet, you'll see it here.",
      //         style: TextStyle(
      //             fontSize: 16,
      //             fontWeight: FontWeight.w400,
      //             color: kDGreyColor),
      //         textAlign: TextAlign.center,
      //       ),
      //     ),
      //     Spacer(
      //       flex: 8,
      //     )
      //   ],
      // ),
    );
  }
}

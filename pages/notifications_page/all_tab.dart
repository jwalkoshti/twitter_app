import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/pages/tweet_pages/detail_tweet_page.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/notification_tile.dart';

class AllNotifications extends StatelessWidget {
  const AllNotifications({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return Container(
      child: ListView(
        children: [
          NotificationTile(
            userName: "@zebraXYZ",
            name: "Zebra ",
            onTap: () {
              rightToLeftNavigation(
                context,
                DetailTweetPage(
                    tweet: dataServices.getTweetListByUserName("@zebraXYZ")[0]),
              );
            },
            notificationType: "has added a tweet",
          ),
          NotificationTile(
            userName: "@cartoonOp",
            name: "Cartoon ",
            onTap: () {
              rightToLeftNavigation(
                context,
                DetailTweetPage(
                    tweet:
                        dataServices.getTweetListByUserName("@cartoonOP")[0]),
              );
            },
            notificationType: "has added a tweet",
          ),
          NotificationTile(
            userName: "@zebraXYZ",
            name: "Zebra ",
            onTap: () {
              rightToLeftNavigation(
                context,
                DetailTweetPage(
                    tweet: dataServices.getTweetListByUserName("@zebraXYZ")[1]),
              );
            },
            notificationType: "has liked your comment",
          ),
          Container(
            color: kELGreyColor,
            height: 1.0,
          ),
        ],
      ),
    );
  }
}

// class MoreNotificationTile extends StatelessWidget {
//   final String userName;
//   final String name;
//   final String notificationType;
//   const MoreNotificationTile({
//     Key key,
//     this.userName,
//     this.name,
//     this.notificationType,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         Navigator.push(
//             context,
//             PageTransition(
//                 type: PageTransitionType.rightToLeftWithFade,
//                 child: MoreNoitficationScreen()));
//       },
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           Container(
//             color: kELGreyColor,
//             height: 1.0,
//           ),
//           Container(
//             margin: EdgeInsets.only(left: 30, top: 10, bottom: 10),
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Row(
//                   children: [
//                     Container(
//                       padding: EdgeInsets.only(right: 20),
//                       child: SvgPicture.asset(
//                         "assets/images/selectedNotificationIcon.svg",
//                         color: kBlueColor,
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(right: 10),
//                       width: 40,
//                       height: 40,
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(90),
//                           color: kLGreyColor),
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.person,
//                           color: Colors.white,
//                           size: 20,
//                         ),
//                         onPressed: () {},
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(right: 10),
//                       width: 40,
//                       height: 40,
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(90),
//                           color: kLGreyColor),
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.person,
//                           color: Colors.white,
//                           size: 20,
//                         ),
//                         onPressed: () {},
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(right: 10),
//                       width: 40,
//                       height: 40,
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(90),
//                           color: kLGreyColor),
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.person,
//                           color: Colors.white,
//                           size: 20,
//                         ),
//                         onPressed: () {},
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(right: 10),
//                       width: 40,
//                       height: 40,
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(90),
//                           color: kLGreyColor),
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.person,
//                           color: Colors.white,
//                           size: 20,
//                         ),
//                         onPressed: () {},
//                       ),
//                     ),
//                     Spacer(),
//                     IconButton(
//                       icon: Icon(
//                         Icons.more_vert,
//                         size: 20,
//                         color: kDGreyColor,
//                       ),
//                       onPressed: () {},
//                     )
//                   ],
//                 ),
//                 Container(
//                   margin: EdgeInsets.only(left: 40),
//                   width: MediaQuery.of(context).size.width,
//                   child: RichText(
//                       text: TextSpan(children: [
//                         TextSpan(
//                             text: "New Tweet notifications for ",
//                             style: TextStyle(
//                                 color: kBlackColor,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.w400)),
//                         TextSpan(
//                             text: "Jwal Koshti",
//                             style: TextStyle(
//                                 color: kBlackColor,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.bold)),
//                         TextSpan(
//                             text: " and 5 others",
//                             style: TextStyle(
//                                 color: kBlackColor,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.w400)),
//                       ])),
//                 )
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }



import 'package:flutter/material.dart';
import 'package:twitter_application/services/constants.dart';

class MessagePage extends StatefulWidget {
  const MessagePage({Key key}) : super(key: key);
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Spacer(
            flex: 10,
          ),
          Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                "Send a message, get a message",
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: kBlackColor),
                textAlign: TextAlign.center,
              )),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            child: Text(
              "Direct Messages are private conversations between you and Other people on Twitter. Share Tweets, media, and more!",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: kDGreyColor),
              textAlign: TextAlign.center,
            ),
          ),
          Spacer(
            flex: 8,
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';
import 'package:twitter_application/widget_models/bottom_sheet_tile.dart';
import 'package:twitter_application/widget_models/circle_share_button.dart';
import 'package:twitter_application/widget_models/tweet_model.dart';

import 'add_reply_screen.dart';

class DetailTweetPage extends StatefulWidget {
  final TweetData tweet;
  const DetailTweetPage({
    Key key,
    this.tweet,
  }) : super(key: key);
  @override
  _DetailTweetPageState createState() => _DetailTweetPageState();
}

class _DetailTweetPageState extends State<DetailTweetPage>
    with TickerProviderStateMixin {
  AnimationController _controller1;
  AnimationController _controller2;
  Animation<Offset> _offsetAnimation1;
  Animation<Offset> _offsetAnimation2;
  String commentInputText;
  FocusNode _focus;
  TextEditingController _controller;
  @override
  void initState() {
    super.initState();

    _focus = FocusNode();
    commentInputText = "";
    _controller = TextEditingController();
    _controller1 = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _controller2 = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _offsetAnimation1 = TweenSequence([
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 0), end: Offset(0, -1.2)), weight: 1),
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 1.2), end: Offset(0, 0)), weight: 1)
    ]).animate(CurvedAnimation(parent: _controller1, curve: Curves.easeInOut));
    _offsetAnimation2 = TweenSequence([
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 0), end: Offset(0, -1.2)), weight: 1),
      TweenSequenceItem(
          tween: Tween(begin: Offset(0, 1.2), end: Offset(0, 0)), weight: 1)
    ]).animate(CurvedAnimation(parent: _controller2, curve: Curves.easeInOut));
  }

  void numberAnimation1() {
    _controller1.forward().whenComplete(() => _controller1.reset());
  }

  void numberAnimation2() {
    _controller2.forward().whenComplete(() => _controller2.reset());
  }

  bool isTextFieldEmpty = true;
  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: true);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.arrow_back_sharp,
              color: kBlueColor,
            )),
        title: Text(
          "Tweet",
          style: TextStyle(color: kBlackColor),
        ),
        bottom: PreferredSize(
          child: Container(
            color: kELGreyColor,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0),
        ),
      ),
      body: ListView(
        physics: ScrollPhysics(),
        shrinkWrap: true,
        children: [
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: kELGreyColor), color: Colors.white),
            child: Material(
              color: Colors.white,
              child: InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.only(top: 10, right: 10, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(90),
                                color: kLGreyColor),
                            child: IconButton(
                              icon: Icon(
                                Icons.person,
                                color: Colors.white,
                                size: 35,
                              ),
                              onPressed: () {},
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    widget.tweet.fullName,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 4.0),
                                  child: Text(
                                    widget.tweet.userName,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: kDGreyColor),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.more_vert,
                              size: 20,
                              color: kDGreyColor,
                            ),
                            onPressed: () {
                              showModalBottomSheet(
                                backgroundColor: Colors.transparent,
                                context: context,
                                builder: (context) => Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                          topRight: Radius.circular(20))),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Center(
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10),
                                          width: 30,
                                          height: 5,
                                          decoration: BoxDecoration(
                                              color: kELGreyColor,
                                              borderRadius:
                                                  BorderRadius.circular(90)),
                                        ),
                                      ),
                                      BottomSheetTile(
                                          text:
                                              "Follow ${widget.tweet.userName}",
                                          iconString:
                                              "assets/icons/followIcon.svg",
                                          ontap: () {}),
                                      BottomSheetTile(
                                          text: "Add/remove from Lists",
                                          iconString:
                                              "assets/icons/addListIcon.svg",
                                          ontap: () {}),
                                      BottomSheetTile(
                                          text:
                                              "Mute  ${widget.tweet.userName}",
                                          iconString:
                                              "assets/icons/muteIcon.svg",
                                          ontap: () {}),
                                      BottomSheetTile(
                                          text:
                                              "Block  ${widget.tweet.userName}",
                                          iconString:
                                              "assets/icons/blockIcon.svg",
                                          ontap: () {}),
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          child: Divider(
                                            height: 1,
                                          )),
                                      BottomSheetTile(
                                          text: "Report Tweet",
                                          iconString:
                                              "assets/icons/reportTweetIcon.svg",
                                          ontap: () {}),
                                      SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          )
                        ],
                      ),
                      widget.tweet.isAComment
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(top: 10.0, bottom: 5),
                              child: Row(
                                children: [
                                  Text(
                                    "Replying to ",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: kDGreyColor),
                                  ),
                                  Text(
                                    widget.tweet.replyingTo,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: kBlueColor),
                                  )
                                ],
                              ),
                            )
                          : Container(
                              height: 10,
                            ),
                      Text(
                        widget.tweet.tweetString,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: kBlackColor),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        color: kELGreyColor,
                        height: 1.0,
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                        child: Row(
                          children: [
                            SlideTransition(
                              position: _offsetAnimation1,
                              child: Text(
                                widget.tweet.reTweetCount.toString(),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: kBlackColor),
                              ),
                            ),
                            Text(
                              " Retweets",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: kDGreyColor),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            SlideTransition(
                              position: _offsetAnimation2,
                              child: Text(
                                widget.tweet.likedCount.toString(),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: kBlackColor),
                              ),
                            ),
                            Text(
                              " Likes",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: kDGreyColor),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: kELGreyColor,
                        height: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              bottomToTopNavigation(
                                context,
                                AddReplyScreen(tweetData: widget.tweet),
                              );
                            },
                            icon: SvgPicture.asset(
                              "assets/icons/replyIcon.svg",
                              color: widget.tweet.isAComment
                                  ? kDGreyColor
                                  : kDGreyColor,
                              width: 20,
                            ),
                          ),
                          // Retweet Button
                          IconButton(
                            onPressed: () {
                              showModalBottomSheet(
                                backgroundColor: Colors.transparent,
                                context: context,
                                builder: (context) => Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                          topRight: Radius.circular(20))),
                                  child: !widget.tweet.isReTweeted
                                      ? Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Center(
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 10),
                                                width: 30,
                                                height: 5,
                                                decoration: BoxDecoration(
                                                    color: kELGreyColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            90)),
                                              ),
                                            ),
                                            BottomSheetTile(
                                                text: "Retweet",
                                                iconString:
                                                    "assets/icons/reTweetIcon.svg",
                                                ontap: () {
                                                  numberAnimation1();
                                                  dataServices.reTweetClicked(
                                                      widget.tweet);
                                                  Navigator.pop(context);
                                                }),
                                            BottomSheetTile(
                                                text: "Quote Tweet",
                                                iconString:
                                                    "assets/icons/quoteTweetIcon.svg",
                                                ontap: () {}),
                                            SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        )
                                      : Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Center(
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 10),
                                                width: 30,
                                                height: 5,
                                                decoration: BoxDecoration(
                                                    color: kELGreyColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            90)),
                                              ),
                                            ),
                                            BottomSheetTile(
                                                text: "Undo Retweet",
                                                iconString:
                                                    "assets/icons/reTweetIcon.svg",
                                                ontap: () {
                                                  numberAnimation1();
                                                  Navigator.pop(context);
                                                  dataServices.reTweetClicked(
                                                      widget.tweet);
                                                }),
                                            SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                ),
                              );
                            },
                            icon: SvgPicture.asset(
                              "assets/icons/reTweetIcon.svg",
                              color: widget.tweet.isReTweeted
                                  ? kGreenColor
                                  : kDGreyColor,
                              width: 20,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              numberAnimation2();
                              dataServices.setLike(widget.tweet);
                            },
                            icon: !widget.tweet.isLiked
                                ? SvgPicture.asset(
                                    "assets/icons/likeIcon.svg",
                                    color: kDGreyColor,
                                    width: 20,
                                  )
                                : SvgPicture.asset(
                                    "assets/icons/likedIcon.svg",
                                    color: kPinkColor,
                                    width: 20,
                                  ),
                          ),
                          // SHARE_BUTTON
                          IconButton(
                            onPressed: () {
                              showModalBottomSheet(
                                backgroundColor: Colors.transparent,
                                context: context,
                                builder: (context) => Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                          topRight: Radius.circular(20))),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Center(
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10),
                                          width: 30,
                                          height: 5,
                                          decoration: BoxDecoration(
                                              color: kELGreyColor,
                                              borderRadius:
                                                  BorderRadius.circular(90)),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "Share Tweet",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                                color: kBlackColor),
                                          ),
                                        ),
                                      ),
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          child: Divider(
                                            height: 1,
                                          )),
                                      BottomSheetTile(
                                          text: "Send via Direct Message",
                                          iconString: "assets/icons/dmIcon.svg",
                                          ontap: () {}),
                                      BottomSheetTile(
                                          text: "Share in a Fleet",
                                          iconString:
                                              "assets/icons/reTweetIcon.svg",
                                          ontap: () {}),
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          child: Divider(
                                            height: 1,
                                          )),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 15, horizontal: 20),
                                        child: Row(
                                          children: [
                                            CircleShareButton(
                                              iconString:
                                                  "assets/icons/addToBookmarks.svg",
                                              buttonName: "Bookmark",
                                              onTap: () {},
                                            ),
                                            CircleShareButton(
                                              iconString:
                                                  "assets/icons/copyLinkIcon.svg",
                                              buttonName: "Copy Link",
                                              onTap: () {},
                                            ),
                                            CircleShareButton(
                                              iconString:
                                                  "assets/icons/shareIcon.svg",
                                              buttonName: "Share via",
                                              onTap: () {},
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          child: Divider(
                                            height: 1,
                                          )),
                                      SizedBox(
                                        height: 100,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            icon: SvgPicture.asset(
                              "assets/icons/shareIcon.svg",
                              color: widget.tweet.isShared
                                  ? kBlueColor
                                  : kDGreyColor,
                              width: 20,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        color: kELGreyColor,
                        height: 0.5,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          // List of Comments

          ListView.builder(
              physics: ScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.tweet.commentsList.length,
              itemBuilder: (context, index) =>
                  widget.tweet.commentsList.length != 0
                      ? Container(
                          child: Tweet(
                            tweetData: widget.tweet.commentsList[index],
                          ),
                        )
                      : Container()),

          SizedBox(height: 180),
        ],
      ),
      bottomSheet: BottomSheet(
        elevation: 0,
        enableDrag: false,
        onClosing: () {},
        builder: (context) => Container(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                color: kELGreyColor,
                height: 1.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12, top: 12),
                child: Row(
                  children: [
                    Text(
                      "Replying to ",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: kDGreyColor),
                    ),
                    Text(
                      widget.tweet.userName,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: kBlueColor),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5),
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                child: TextField(
                  controller: _controller,
                  keyboardType: TextInputType.multiline,
                  focusNode: _focus,
                  maxLines: null,
                  onChanged: (value) {
                    setState(() {
                      commentInputText = value;
                      isTextFieldEmpty = value == "" ? true : false;
                    });
                  },
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: kBlackColor),
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                          onPressed: () {
                            bottomToTopNavigation(
                              context,
                              AddReplyScreen(
                                tweetData: widget.tweet,
                                initialInputText: commentInputText,
                              ),
                            );
                            _controller.clear();
                            _focus.unfocus();
                          },
                          icon: Icon(Icons.open_in_full_rounded),
                          color: kBlueColor),
                      border: InputBorder.none,
                      hintText: "Tweet your reply",
                      hintStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: kDGreyColor)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12),
                color: kBlueColor,
                height: 2.0,
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    child: SvgPicture.asset(
                      "assets/icons/addPicture.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    child: SvgPicture.asset(
                      "assets/icons/addGIF.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Spacer(),
                  IconButton(
                      color: kBlueColor,
                      disabledColor: kBlueColor.withOpacity(0.5),
                      icon: Icon(Icons.brightness_1_outlined,
                          color: kELGreyColor),
                      onPressed: isTextFieldEmpty ? null : () {}),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    width: 65,
                    height: 33,
                    child: Material(
                      color: isTextFieldEmpty
                          ? kBlueColor.withOpacity(0.5)
                          : kBlueColor,
                      borderRadius: BorderRadius.circular(90),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(90),
                        onTap: isTextFieldEmpty
                            ? null
                            : () {
                                _focus.unfocus();
                                _controller.clear();
                                dataServices.addCommentToTweet(
                                    widget.tweet, commentInputText);
                              },
                        child: Center(
                          child: Text("Reply",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

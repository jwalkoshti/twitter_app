import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';
import 'package:twitter_application/services/constants.dart';
import 'package:twitter_application/services/data_services.dart';

class AddReplyScreen extends StatefulWidget {
  final TweetData tweetData;
  final String initialInputText;
  const AddReplyScreen({Key key, this.initialInputText, this.tweetData})
      : super(key: key);
  @override
  _AddReplyScreenState createState() => _AddReplyScreenState();
}

class _AddReplyScreenState extends State<AddReplyScreen> {
  String replyingText;
  bool isTextFieldEmpty;
  TextEditingController _controller;
  @override
  void initState() {
    // implement initState
    super.initState();
    replyingText = widget.initialInputText == "" ? "" : widget.initialInputText;
    _controller = TextEditingController(text: replyingText);
    isTextFieldEmpty = true;
  }

  @override
  Widget build(BuildContext context) {
    final dataServices = Provider.of<DataServices>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: SvgPicture.asset(
            "assets/icons/cancelButtonIcon.svg",
            color: kBlueColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        bottom: PreferredSize(
          child: Container(
            color: kELGreyColor,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0),
        ),
        actions: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            width: 80,
            child: Material(
              color:
                  isTextFieldEmpty ? kBlueColor.withOpacity(0.5) : kBlueColor,
              borderRadius: BorderRadius.circular(90),
              child: InkWell(
                borderRadius: BorderRadius.circular(90),
                onTap: isTextFieldEmpty
                    ? null
                    : () {
                        dataServices.addCommentToTweet(
                            widget.tweetData, replyingText);
                        _controller.clear();
                        Navigator.of(context).pop();
                      },
                child: Center(
                  child: Text("Reply",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
          )
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 28, vertical: 0),
                        padding: EdgeInsets.symmetric(vertical: 10),
                        color: kLGreyColor,
                        width: 2,
                        height: 50,
                      ),
                      SizedBox(
                        width: 3,
                      ),
                      Text(
                        "Replying to ",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: kDGreyColor),
                      ),
                      Text(
                        widget.tweetData.userName,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: kBlueColor),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 45),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(9, 0, 10, 0),
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadiusDirectional.circular(100),
                            color: kLGreyColor),
                        child: Icon(
                          Icons.person_rounded,
                          color: kDGreyColor,
                          size: 30,
                        ),
                      ),
                      Expanded(
                        child: TextField(
                          controller: _controller,
                          autofocus: true,
                          onChanged: (text) {
                            setState(() {
                              replyingText = text;
                              isTextFieldEmpty = text == "" ? true : false;
                            });
                          },
                          style: TextStyle(
                              fontSize: 21, fontWeight: FontWeight.w400),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Tweet your reply",
                            hintStyle: TextStyle(
                                color: kDGreyColor,
                                fontSize: 18,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Spacer(),
            Divider(
              height: 1,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5, top: 0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: SvgPicture.asset(
                      "assets/icons/addPicture.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: SvgPicture.asset(
                      "assets/icons/addGIF.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: SvgPicture.asset(
                      "assets/icons/addPoll.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: SvgPicture.asset(
                      "assets/icons/addSchedule.svg",
                      color: kBlueColor,
                      width: 25,
                    ),
                  ),
                  Spacer(),
                  IconButton(
                      color: kBlueColor,
                      disabledColor: kBlueColor.withOpacity(0.5),
                      icon: Icon(Icons.add_circle),
                      onPressed: isTextFieldEmpty ? null : () {})
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

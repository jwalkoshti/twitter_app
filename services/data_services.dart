import 'package:flutter/material.dart';
import 'package:twitter_application/data_models/tweet_data_model.dart';

class DataServices with ChangeNotifier {
  String loggedInName;
  String loggedInUserName;
  List<TweetData> homeTweetList = [
    TweetData(
      replyingTo: "",
      fullName: "Jwal Koshti",
      userName: "@jwalkoshti",
      commentsList: [
        TweetData(
          replyingTo: "@jwalkoshti",
          fullName: "Zebra Sharma",
          userName: "@zebraXYZ",
          commentsList: [],
          likedCount: 26,
          reTweetCount: 12,
          isAComment: true,
          isLiked: false,
          isShared: false,
          isReTweeted: false,
          tweetString: "Lorem ipsum dolor sit amet, consectetur ",
          tweetCreated: DateTime.now(),
        ),
      ],
      likedCount: 124,
      reTweetCount: 26,
      isAComment: false,
      isLiked: false,
      isShared: false,
      isReTweeted: false,
      tweetString:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      tweetCreated: DateTime.now(),
    ),
    TweetData(
      replyingTo: "",
      fullName: "Zebra Sharma",
      userName: "@zebraXYZ",
      commentsList: [
        TweetData(
          replyingTo: "@zebraXYZ",
          fullName: "Jwal Koshti",
          userName: "@jwalkoshti",
          commentsList: [],
          likedCount: 23,
          reTweetCount: 12,
          isAComment: true,
          isLiked: false,
          isShared: false,
          isReTweeted: false,
          tweetString:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
          tweetCreated: DateTime.now(),
        ),
      ],
      likedCount: 234,
      reTweetCount: 65,
      isAComment: false,
      isLiked: false,
      isShared: false,
      isReTweeted: false,
      tweetString:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      tweetCreated: DateTime.now(),
    ),
    TweetData(
      replyingTo: "",
      fullName: "Cartoon Hero",
      userName: "@cartoonOP",
      commentsList: [],
      likedCount: 234,
      reTweetCount: 56,
      isAComment: false,
      isLiked: false,
      isShared: false,
      isReTweeted: false,
      tweetString:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      tweetCreated: DateTime.now(),
    ),
  ];

  DataServices({this.loggedInName, this.loggedInUserName});

  List<TweetData> getMyTweetList() {
    List<TweetData> myTweetList = [];
    homeTweetList.map((tweet) {
      if (tweet.fullName == loggedInName) {
        myTweetList.add(tweet);
      }
    }).toList();
    return myTweetList;
  }

  List<TweetData> getMyCommentedTweetList() {
    List<TweetData> myCommentedTweetList = [];
    homeTweetList.forEach((element) {
      element.commentsList.map((tweet) {
        if (tweet.fullName == loggedInName) {
          myCommentedTweetList.add(tweet);
        }
        if (tweet.commentsList.length != 0) {
          tweet.commentsList.forEach((element) {
            if (element.fullName == loggedInName) {
              myCommentedTweetList.add(element);
            }
          });
        }
      }).toList();
    });
    return myCommentedTweetList;
  }

  List<TweetData> getMyMentionsNotificationList(userName) {
    List<TweetData> myMentionsNotificationTweetList = [];
    homeTweetList.forEach((element) {
      if (element.userName == userName) {
        element.commentsList.map((tweet) {
          if (tweet.userName == loggedInUserName) {
            myMentionsNotificationTweetList.add(tweet);
          }
        }).toList();
      }
    });
    return myMentionsNotificationTweetList;
  }

  List<TweetData> getTweetListByUserName(userName) {
    List<TweetData> tweetListByUserName = [];
    homeTweetList.forEach((tweet) {
      if (tweet.userName == userName) {
        tweetListByUserName.add(tweet);
      }
      tweet.commentsList.forEach((tweet) {
        if (tweet.userName == userName) {
          tweetListByUserName.add(tweet);
        }
      });
    });
    return tweetListByUserName;
  }

  void setLike(TweetData tweet) {
    tweet.isLiked = !tweet.isLiked;
    tweet.isLiked ? tweet.likedCount++ : tweet.likedCount--;
    notifyListeners();
  }

  void reTweetClicked(TweetData tweet) {
    tweet.isReTweeted = !tweet.isReTweeted;
    tweet.isReTweeted ? tweet.reTweetCount++ : tweet.reTweetCount--;
    notifyListeners();
  }

  void addCommentToTweet(TweetData tweetData, String inputText) {
    tweetData.commentsList.add(TweetData(
        replyingTo: tweetData.userName,
        isLiked: false,
        isReTweeted: false,
        isAComment: true,
        isShared: false,
        tweetString: inputText,
        fullName: loggedInName,
        userName: loggedInUserName,
        commentsList: [],
        reTweetCount: 0,
        likedCount: 0,
        tweetCreated: DateTime.now()));
    notifyListeners();
  }

  void addToHomeTweetList(inputText) {
    homeTweetList.insert(
        0,
        TweetData(
            replyingTo: "",
            isLiked: false,
            isReTweeted: false,
            isAComment: false,
            isShared: false,
            tweetString: inputText,
            fullName: loggedInName,
            userName: loggedInUserName,
            commentsList: [],
            reTweetCount: 0,
            likedCount: 0,
            tweetCreated: DateTime.now()));
    notifyListeners();
  }
}

import 'package:flutter/material.dart';

Color kBlueColor = Color.fromRGBO(29, 161, 242, 1);
Color kDBlueColor = Color.fromRGBO(32, 83, 112, 1);
Color kBlackColor = Color.fromRGBO(20, 23, 26, 1);
Color kDGreyColor = Color.fromRGBO(101, 119, 134, 1);
Color kLGreyColor = Color.fromRGBO(170, 184, 194, 1);
Color kELGreyColor = Color.fromRGBO(225, 232, 237, 1);
Color kEELGreyColor = Color.fromRGBO(245, 248, 250, 1);
Color kPinkColor = Color.fromRGBO(224, 36, 94, 1);
Color kGreenColor = Color.fromRGBO(23, 191, 99, 1);
const List<String> searchScreenTabsList = [
  "For you",
  "COVID-19",
  "Trending",
  "News",
  "Sports",
  "Entertainment"
];
const List<String> notificationScreenTabsList = ["All", "Mentions"];
const List<String> profileScreenTabList = [
  "Tweets",
  "Tweets & replies",
  "Media",
  "Likes"
];

//  Routing Constant Models

void bottomToTopNavigation(
  context,
  Widget page,
) {
  Navigator.push(
    context,
    PageRouteBuilder<dynamic>(
        transitionDuration: Duration(milliseconds: 300),
        reverseTransitionDuration: Duration(milliseconds: 300),
        pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) =>
            page,
        transitionsBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
        ) {
           final Tween<double> scaleTween = Tween<double>(begin: 1.0, end: 0.9);
          final Tween<double> fadeTween = Tween<double>(begin: 1, end: 0.8);
          final Animation<double> _pageScaleAnimation = scaleTween.animate(
              CurvedAnimation(parent: secondaryAnimation, curve: Curves.easeOutSine));
          final Animation<double> _pageFadeAnimation = fadeTween.animate(
              CurvedAnimation(parent: secondaryAnimation, curve: Curves.easeOutSine));
          final Tween<Offset> offsetTween =
              Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset(0.0, 0.0));
          final Animation<Offset> _animation = offsetTween
              .animate(CurvedAnimation(parent: animation, curve: Curves.easeInSine));
          return SlideTransition(
            position: _animation,
            child: FadeTransition(
              opacity: _pageFadeAnimation,
              child: ScaleTransition(
                scale: _pageScaleAnimation,
                child: child,
              ),
            ),
          );
        }),
  );
}

void rightToLeftNavigation(context, Widget page) {
  Navigator.push(
    context,
    PageRouteBuilder<dynamic>(
        transitionDuration: Duration(milliseconds: 300),
        reverseTransitionDuration: Duration(milliseconds: 300),
        pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) =>
            page,
        transitionsBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
        ) {
          final Tween<double> scaleTween = Tween<double>(begin: 1.0, end: 0.9);
          final Tween<double> fadeTween = Tween<double>(begin: 1, end: 0.8);
          final Animation<double> _pageScaleAnimation = scaleTween.animate(
              CurvedAnimation(parent: secondaryAnimation, curve: Curves.easeOutSine));
          final Animation<double> _pageFadeAnimation = fadeTween.animate(
              CurvedAnimation(parent: secondaryAnimation, curve: Curves.easeOutSine));
          final Tween<Offset> offsetTween =
              Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0));
          final Animation<Offset> _animation = offsetTween
              .animate(CurvedAnimation(parent: animation, curve: Curves.easeInSine));
          return SlideTransition(
            position: _animation,
            child: FadeTransition(
              opacity: _pageFadeAnimation,
              child: ScaleTransition(
                scale: _pageScaleAnimation,
                child: child,
              ),
            ),
          );
        }),
  );
}

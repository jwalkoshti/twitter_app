import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'pages/drawer_page/app_drawer.dart';
import 'pages/home_page/home_page.dart';
import 'pages/messages_page/message_page.dart';
import 'pages/messages_page/person_search_screen.dart';
import 'pages/notifications_page/notification_page.dart';
import 'pages/search_page/search_page.dart';
import 'pages/search_page/search_twitter_screen.dart';
import 'pages/fab_pages/add_message_screen.dart';
import 'pages/fab_pages/add_tweet_screen.dart';
import 'services/constants.dart';
import 'widget_models/app_bar_models/heading_app_bar.dart';
import 'widget_models/app_bar_models/search_appbar.dart';
import 'widget_models/app_bar_models/twitter_icon_app_bar.dart';

class AppScreen extends StatefulWidget {
  @override
  _AppScreenState createState() => _AppScreenState();
}

GlobalKey<ScaffoldState> _scKey = GlobalKey();

class _AppScreenState extends State<AppScreen> with TickerProviderStateMixin {
  List<Widget> _pagesList;
  List<Widget> _appBarItemList;
  int _currentIndex;
  AnimationController _fabController;
  Animation _fabRotationAnimation;
  Animation _fabScaleAnimation;

  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
    _pagesList = [HomePage(), SearchPage(), NotificationPage(), MessagePage()];
    _appBarItemList = [
      TwitterIcon(),
      SearchBar(
        hintText: "Search Twitter",
        page: SearchTwitterScreen(),
      ),
      HeadingAppBar(title: "Notifications"),
      SearchBar(
        hintText: "Search People",
        page: SearchPersonScreen(),
      )
    ];
    _fabController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _fabRotationAnimation = Tween(begin: -90 / 360, end: 0 / 360).animate(
        CurvedAnimation(parent: _fabController, curve: Curves.easeInOut));
    _fabScaleAnimation = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 1.2), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.2, end: 1.0), weight: 1)
    ]).animate(
        CurvedAnimation(parent: _fabController, curve: Curves.easeInOut));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      drawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: MediaQuery.of(context).size.width * 0.25,
      backgroundColor: Colors.white,
      key: _scKey,
      drawer: AppDrawer(),
      appBar: AppBar(
        bottom: _currentIndex == 0 || _currentIndex == 3
            ? PreferredSize(
                child: Container(
                  color: kELGreyColor,
                  height: 1.0,
                ),
                preferredSize: Size.fromHeight(1.0),
              )
            : PreferredSize(
                child: Container(), preferredSize: Size.fromHeight(0)),
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: kBlueColor,
        ),
        centerTitle: _currentIndex != 3,
        leading: IconButton(
          icon: Icon(
            Icons.menu_rounded,
            size: 36.0,
          ),
          onPressed: () => _scKey.currentState.openDrawer(),
        ),
        title: _appBarItemList[_currentIndex],
        actions: [
          IconButton(
            icon: _currentIndex == 0
                ? SvgPicture.asset(
                    "assets/icons/timelineStarIcon.svg",
                    color: kBlueColor,
                  )
                : SvgPicture.asset(
                    "assets/icons/settingsIcon.svg",
                    color: kBlueColor,
                  ),
            onPressed: _currentIndex == 0 ? () {} : () {},
          ),
        ],
      ),
      body: _pagesList[_currentIndex],
      bottomNavigationBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: Container(
            decoration: BoxDecoration(border: Border.all(color: kELGreyColor)),
            child: BottomNavigationBar(
                elevation: 0,
                backgroundColor: Colors.white,
                type: BottomNavigationBarType.fixed,
                selectedItemColor: kBlueColor,
                unselectedItemColor: kDGreyColor,
                currentIndex: _currentIndex,
                onTap: (index) {
                  setState(() {
                    _currentIndex = index;
                  });
                  index == 3
                      ? _fabController.forward()
                      : _fabController.reverse();
                },
                showSelectedLabels: false,
                showUnselectedLabels: false,
                items: [
                  BottomNavigationBarItem(
                      activeIcon: SvgPicture.asset(
                        "assets/icons/selectedHomeIcon.svg",
                        color: kBlueColor,
                      ),
                      icon: SvgPicture.asset(
                        "assets/icons/homeIcon.svg",
                      ),
                      label: "Home"),
                  BottomNavigationBarItem(
                      activeIcon: SvgPicture.asset(
                        "assets/icons/selectedSearchIcon.svg",
                        color: kBlueColor,
                      ),
                      icon: SvgPicture.asset(
                        "assets/icons/searchIcon.svg",
                      ),
                      label: "Search"),
                  BottomNavigationBarItem(
                      activeIcon: SvgPicture.asset(
                        "assets/icons/selectedNotificationIcon.svg",
                        color: kBlueColor,
                      ),
                      icon: SvgPicture.asset(
                        "assets/icons/notificationIcon.svg",
                      ),
                      label: "Notification"),
                  BottomNavigationBarItem(
                      activeIcon: SvgPicture.asset(
                        "assets/icons/selectedMessageIcon.svg",
                        color: kBlueColor,
                      ),
                      icon: SvgPicture.asset(
                        "assets/icons/messageIcon.svg",
                      ),
                      label: "Messaging"),
                ]),
          )),
      floatingActionButton: ScaleTransition(
        scale: _fabScaleAnimation,
        child: RotationTransition(
          turns: _fabRotationAnimation,
          child: FloatingActionButton(
            onPressed: () {
              _currentIndex == 3
                  ? rightToLeftNavigation(context, AddMessageScreen())
                  : bottomToTopNavigation(context, AddTweet());
            },
            child: _currentIndex == 3
                ? SvgPicture.asset(
                    "assets/icons/writeMessageIcon.svg",
                    color: Colors.white,
                  )
                : RotatedBox(
                    quarterTurns: 45,
                    child: SvgPicture.asset(
                      "assets/icons/writeTweetIcon.svg",
                      color: Colors.white,
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
